#!/usr/bin/env python

import re
import sys
import subprocess
import json
from os import chmod

CONFIGFILE = "/opt/centreon_trap_relay/config.json"

# read config
with open(CONFIGFILE) as json_file:
    config_data = json.load(json_file)

generic_config = config_data["generic"]

try:
    LOG_FILE = generic_config["LOG_FILE"]
    LOG = generic_config["LOG"]
    CREATE_CRONJOB = generic_config["CREATE_CRONJOB"]
    NSCA_HOST = generic_config["NSCA_HOST"]
    NSCA_PORT = generic_config["NSCA_PORT"]
    NSCA_CONFIG = generic_config["NSCA_CONFIG"]
except Exception as e:
    print(f"UNKNOWN - FAILED TO PARSE CONFIG FILE {e}")
    exit(3)

# read raw snmp message
trap = sys.stdin.readlines()
trap.pop(0)
r = "".join(trap)

# check which config to take by checking "match"
config = None
for data in config_data["specific"]:
    match_words = data["MATCH"]
    for match in match_words:
        if match in r:
            config = data

if config is None:
    print("UNKNOWN - NO MATCH FOUND IN CONFIG FILE.")
    exit(3)

try:
    ID_FIELD_OID = config["ID_FIELD_OID"]
    INFO_FIELD_OID = config["INFO_FIELD_OID"]
    EXTENDED_INFO_FIELD_OID = config["EXTENDED_INFO_FIELD_OID"]
    ALARM_TYPE_FIELD_OID = config["ALARM_TYPE_FIELD_OID"]
    EXTENDED_ALARM_TYPE_FIELD_OID = config["EXTENDED_ALARM_TYPE_FIELD_OID"]
    ALARM_MAP = config["ALARM_MAP"]
    EXTENDED_ALARM_MAP = config["EXTENDED_ALARM_MAP"]
    SAVE_FILE = config["SAVE_FILE"]
    NSCA_IP = config["IP"]
    COMMAND = config["COMMAND"]
except Exception as e:
    print(f"UNKNOWN - FAILED TO PARSE CONFIG FILE {e}")
    exit(3)

# write raw snmp message to log
if LOG:
    f = open(LOG_FILE, "a")
    f.write("########################\nRaw:\n" + str(r) + "\n")
    f.close()

msg_id = info = extended_info = alarm = extended_alarm = None

for line in trap:
    if "UDP:" in line:
        IP = re.findall("UDP:.\[([\d.+]+)\]", r)
        IP = str(IP).strip("[]'")
    else:
        if any(id_text in line for id_text in ID_FIELD_OID):
            msg_id = line.split(" ", 1)[1].strip()
        if any(info_text in line for info_text in INFO_FIELD_OID):
            info = line.split(" ", 1)[1].strip()
        if any(e_info_text in line for e_info_text in EXTENDED_INFO_FIELD_OID):
            extended_info = line.split(" ", 1)[1].strip()
        if any(alarm_text in line for alarm_text in ALARM_TYPE_FIELD_OID):
            alarm = line.split(" ", 1)[1].strip()
        if any(e_alarm_text in line for e_alarm_text in EXTENDED_ALARM_TYPE_FIELD_OID):
            extended_alarm = line.split(" ", 1)[1].strip()


try:
    save_file = open(SAVE_FILE, "r")
    data = json.load(save_file)
    save_file.close()
except Exception as e:
    data = {}

if alarm is not None and msg_id is not None and info is not None:
    alarm = alarm.replace('"', "")
    info = info.replace('"', "")

    msg_dict = {
        "alarm": alarm,
        "info": info,
    }

    if extended_alarm is not None:
        extended_alarm = extended_alarm.replace('"', "")
        msg_dict["extended_alarm"] = extended_alarm
    if extended_info is not None:
        extended_info = extended_info.replace('"', "")
        msg_dict["extended_info"] = extended_info

    alarm = next((i for i in ALARM_MAP if i in alarm), None)
    if alarm is not None:
        msg_dict["alarm"] = alarm
        # critical alarm -> append to data
        if ALARM_MAP[alarm] == 2:
            data[msg_id] = msg_dict
        # warning alarm -> append to data
        elif ALARM_MAP[alarm] == 1:
            data[msg_id] = msg_dict
        # resolve / ok alarm -> remove from data or do nothing
        elif ALARM_MAP[alarm] == 0:
            try:
                del data[msg_id]
            except KeyError:
                pass
        # some other alarm e.g. info -> completly leave out
        elif ALARM_MAP[alarm] == 3:
            # send
            pass

save_file = open(SAVE_FILE, "w")
json.dump(data, save_file)
save_file.close()

i = len(data)
code = 0

final_msg = f"{i} Event(s) for {IP}"
MSG = rf"{final_msg} \n"
for event_id in data:
    event = data[event_id]
    msg = rf"{event['info']}"
    if "extended_info" in event:
        msg += rf" - {event['extended_info']}\n"
    else:
        msg += r"\n"
    MSG += msg

    # do checks for return code

    # if the object has the extended_alarm set, use this for return code
    # otherwise use the default alarm as return code
    if "extended_alarm" in event:
        t = next((i for i in EXTENDED_ALARM_MAP if i in event["extended_alarm"]), None)
        if t is not None:
            temp_code = EXTENDED_ALARM_MAP[t]
            if temp_code == 2 or code in (0, 3) and temp_code == 1:
                code = temp_code
    else:
        temp_code = ALARM_MAP[event["alarm"]]
        if temp_code == 2 or code in (0, 3) and temp_code == 1:
            code = temp_code


STATUS_CODE = code

# send_nsca only takes hexadecimal delimiters as args
# tabs dont work here bcs the raw-string attribute ruins it
command = rf"{NSCA_IP}#{COMMAND}#{STATUS_CODE}#'{MSG}'"
p1 = subprocess.Popen(["echo", command], stdout=subprocess.PIPE)
p2 = subprocess.Popen(
    ["send_nsca", "-H", NSCA_HOST, "-p", NSCA_PORT, "-c", NSCA_CONFIG, "-d", "0x23"],
    stdin=p1.stdout,
    stdout=subprocess.PIPE,
)
mycmd = p2.communicate()

NSCA_CMD = f"| send_nsca -H {NSCA_HOST} -p {NSCA_PORT} -c {NSCA_CONFIG} -d 0x23"
ECHO_CMD = rf"echo {NSCA_IP}#{COMMAND}#{STATUS_CODE}#'{MSG}'"

if CREATE_CRONJOB:
    save_cmd_file_name = f"/etc/cron.daily/snmp2nsca-{NSCA_IP}-{COMMAND}"
    f = open(save_cmd_file_name, "w")
    f.write("#!/bin/sh\n")
    f.write(ECHO_CMD + NSCA_CMD)
    f.close()
    chmod(save_cmd_file_name, 0o755)


# write parsed message t log
if LOG:
    f = open(LOG_FILE, "a")
    f.write("Command Executed: \n")
    f.write(ECHO_CMD + NSCA_CMD + "\n")
    f.write(
        "Value returned by Command:\n"
        + str(mycmd[0])
        + "\n###########################################################"
    )
    f.close()
