## Setup SNMP & SNMPTRAPD

do following steps as root

**install required packages**

`apt-get install snmp snmpd snmptrapd snmp-mibs-downloader`

**get the repository**

```
cd /opt
git clone THIS_SITE
cd centreon_trap_relay
```

**copy config files and adjust them if needed**

`cp etc/snmp/* /etc/snmp`

**place files in MIBS folder in /usr/share/snmp/MIBS**

`cp MIBS/* /usr/share/snmp/mibs`

---

## Setup Script snmp2nsca.py and config file

- Requirements: Python 3.9

**configure /etc/snmp/snmptrapd.conf for script**

- Adjust python path & script location
  `traphandle default /usr/bin/python3.9 /opt/centreon_trap_relay/snmp2nsca.py`
- Adjust Authentication Information

  ```
  createUser -e "ENGINEID" "AUTH-USER" "SHA256" "AUTH-PW" "AES" "ENC-PW"

  authUser log,execute,net AUTH-USER

  authCommunity log,execute,net private
  authCommunity log,execute,ne  t public
  ```

  - **createUser & authUser**: SNMPv3
  - **authCommunity**: SNMPv1 & SNMPv2

**adjust config-file**
By default the config file is called "config.json", and is located in the same folder as the script. You can change this behavior by changing the path in the Script: `CONFIGFILE = "config.json"`.
→ The config file consists of 2 parts: generic and specific configurations. Specific configurations look for matches in the SNMP-Message.

- **generic**

  - **LOG** boolean → If logs (raw snmp message, command sent, ...) should be written.
  - **LOG_FILE** str → Where to write those logs.
  - **CREATE_CRONJOB** boolean → If true, the command which was sent to centreon will be saved in a shellscript under /etc/cron.daily, which will then run the script every 24h.
  - **NSCA_HOST** str → Hostname where to sent nsca msg to.
  - **NSCA_PORT** int → The Host Port
  - **NSCA_CONFIG** str → Nsca config

- **specific**
  - **MATCH** [str] → If any string in this array is inside the SNMP-Message received, this config is used.
  - **ID_FIELD_OID** [str] → SNMP-Message-Part, which contains an ID of the message. Matches by substring.
  - **INFO_FIELD_OID** [str] → SNMP-Message-Part, which contains an Information of the message. Matches by substring.
  - **EXTENDED_INFO_FIELD_OID** [str] → SNMP-Message-Part, which contains extended Information of the message. Matches by substring. Not required.
  - **ALARM_TYPE_FIELD_OID** [str] → SNMP-Message-Part, which contains an alarm type of the message. Matches by substring. Meaning of the alarm-type is handled by ALARM_MAP.
  - **EXTENDED_ALARM_TYPE_FIELD_OID** [str] → SNMP-Message-Part, which contains an extended alarm type of the message. Matches by substring. Meaning of the alarm-type is handled by EXTENDED_ALARM_MAP. Not required.
  - **ALARM_MAP** {str:int} → Options that can be returned by ALARM_TYPE_FIELD_OID are mapped to an return code here (0: OK, 1: WARNING, 2: CRITICAL, 3: UNKNOWN)
  - **EXTENDED_ALARM_MAP** {str:int} → Options that can be returned by EXTENDED_ALARM_TYPE_FIELD_OID are mapped to an return code here (0: OK, 1: WARNING, 2: CRITICAL, 3: UNKNOWN). Not required. Extended Alarms take priority over default Alarms.
  - **SAVE_FILE** str → path to a file where data is stored
  - **IP** str → IP-Address for Centreon-Message
  - **COMMAND** str → Command for Centreon-Message

**Troubleshooting**
Check the Service snmptrapd for Error logs: `systemctl status snmptrapd`.
Enable Logging and check the Log-output → Send_nsca return msg.
